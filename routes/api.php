<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\CommentReplayController;
use App\Http\Controllers\InteractionController;
use App\Http\Controllers\PostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('register',[UserController::class,'Register']);
Route::post('login',[UserController::class,'Login']);
Route::post('logout',[UserController::class,'logout']);

Route::get('allUsers',[UserController::class,'GetAll']);
Route::post('addPost',[PostController::class,'Add'])->middleware(['auth:api','scope:user,admin']);
Route::post('updatePost',[PostController::class,'Update'])->middleware(['auth:api','scope:user,admin']);
Route::post('deletePost',[PostController::class,'Delete'])->middleware(['auth:api','scope:user,admin']);
Route::post('allPostToUser',[PostController::class,'allPostToUser']);
Route::post('allToOnePost',[PostController::class,'allToOnePost']);

Route::post('addComment',[CommentController::class,'Add'])->middleware(['auth:api','scope:user,admin']);
Route::post('deleteComment',[CommentController::class,'Delete'])->middleware(['auth:api','scope:user,admin']);
Route::post('allCommentToPost',[CommentController::class,'allCommentToPost']);


Route::post('addReply',[CommentReplayController::class,'Add'])->middleware(['auth:api','scope:user,admin']);
Route::post('deleteReply',[CommentReplayController::class,'Delete'])->middleware(['auth:api','scope:user,admin']);

Route::post('addInteraction',[InteractionController::class,'Add'])->middleware(['auth:api','scope:user,admin']);
Route::post('deleteInteraction',[InteractionController::class,'Delete'])->middleware(['auth:api','scope:user,admin']);
Route::post('allPostInteraction',[InteractionController::class,'allPostInteraction']);