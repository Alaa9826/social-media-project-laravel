<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Comment;


class Post extends Model
{
    use HasFactory;
    protected $fillable=['title','content','postImage','postVideo','user_id'];
    protected $hidden=[];

    /**
     * Get the user that owns the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    /**
     * Get all of the comments for the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }
    /**
     * Get all of the interactions for the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts_Interactions()
    {
        return $this->hasMany(PostInteraction::class, 'post_id');
    }
    /**
     * Get all of the comments for the Post
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function commentsReplaies()
    {
        return $this->hasManyThrough(Comment::class, CommentReplay::class,'comment_id','post_id');
    }
}