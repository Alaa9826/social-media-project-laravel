<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interaction extends Model
{
    use HasFactory;
     protected $fillable=['type'];
    protected $hidden=[];
    /**
     * Get all of the posts_Interactions for the Interaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts_Interactions()
    {
        return $this->hasMany(PostInteraction::class, 'interaction_id');
    }
}