<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostInteraction extends Model
{
    use HasFactory;
     protected $fillable=['user_id','post_id','interaction_id'];
    protected $hidden=[];
    /**
     * Get the user that owns the PostInteraction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    /**
     * Get the user that posts the PostInteraction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
     /**
      * Get the user that owns the PostInteraction
      *
      * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
      */
     public function interactions()
     {
         return $this->belongsTo(Interaction::class, 'interaction_id');
     }
}