<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Post;
use App\Models\CommentReplay;


class Comment extends Model
{
    use HasFactory;
     protected $fillable=['content','user_id','post_id'];
    protected $hidden=[];
    /**
     * Get the user that owns the Comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    /**
     * Get the post that owns the Comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function posts()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
    /**
     * Get all of the commentsReplay for the Comment
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsReplaies()
    {
        return $this->hasMany(CommentReplay::class, 'comment_id');
    }
}