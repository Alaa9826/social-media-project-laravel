<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Comment;

class CommentReplay extends Model
{
    use HasFactory;
       protected $fillable=['content','user_id','comment_id'];
       protected $hidden=[];
    /**
     * Get the users that owns the CommentReplay
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    /**
     * Get the comments that owns the CommentReplay
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function comments()
    {
        return $this->belongsTo(Comment::class, 'comment_id');
    }
}