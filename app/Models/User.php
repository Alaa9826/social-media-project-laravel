<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

use App\Models\Post;
use App\Models\Comment;
use App\Models\CommentReplay;


class User extends Authenticatable
{
    use  HasApiTokens,HasFactory,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];

   /**RelationShips */
   /**
    * Get all of the posts for the User
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function posts()
   {
       return $this->hasMany(Post::class, 'user_id');
   }
 /**
  * Get all of the comments for the User
  *
  * @return \Illuminate\Database\Eloquent\Relations\HasMany
  */
 public function comments()
 {
     return $this->hasMany(Comment::class, 'user_id');
 }
 /**
  * Get all of the commentsReplay for the User
  *
  * @return \Illuminate\Database\Eloquent\Relations\HasMany
  */
 public function commentsReplaies()
 {
     return $this->hasMany(CommentReplay::class, 'user_id');
 }
 /**
  * Get all of the interaction for the User
  *
  * @return \Illuminate\Database\Eloquent\Relations\HasMany
  */
 public function posts_Interactions()
 {
     return $this->hasMany(PostInteraction::class, 'user_id');
 }
}