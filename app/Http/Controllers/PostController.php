<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeletePostRequest;
use App\Http\Requests\PostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Traits\MyUploadFiles;
use App\Http\Traits\UploadFiles;
use App\Models\Comment;
use App\Models\CommentReplay;
use App\Models\Post;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use function PHPSTORM_META\type;
use function PHPUnit\Framework\isEmpty;

class PostController extends Controller
{ 
  use MyUploadFiles;
    public function Add(PostRequest $request){
      
       $postImage= $this->UploadImage($request,'postImage');
       $postVideo= $this->UploadVideo($request,'postVideo');
 $post=[
                  'title' => $request->title,
			            'content' => $request->content,
                  'postImage' => $postImage,
                  'postVideo' => $postVideo,
                  'user_id' =>  $request->user()->id ];
        try{
            
      
            Post::create($post);
            return  response()->json(['message' => 'success add post'], 201);
    }catch(Exception ){
          return  response()->json(['message' => 'error in add post'], 401);}
                     
    }
    public function Update(UpdatePostRequest $request){
        
     $foundPost = Post::find($request->post_id);
     if($foundPost != null){
         if ($foundPost->user_id == $request->user()->id) {
           $postImage= $this->UploadImage($request,'postImage');
           $postVideo= $this->UploadVideo($request,'postVideo');
           $post=[
                  'title' => $request->title,
			            'content' => $request->content,
                  'postImage' => $postImage,
                  'postVideo' => $postVideo ];
           $foundPost->update($post);
            return  response()->json(['message' => 'success update your post'], 202);
          } else {
               return  response()->json(['message' => 'you can not update post because not to you'], 402);
          }
         }else{
                return  response()->json(['message' => 'post not found'], 404);
         }
     }
     public function Delete(DeletePostRequest $request){
     
      $foundPost = Post::find($request->post_id);
      $postImages=$foundPost->postImage;
      $postVideos=$foundPost->postVideo;
            
             
       if($foundPost != null) {
        if ($foundPost->user_id == $request->user()->id || $request->user()->role == 'admin')  {
         
           DB::beginTransaction();
         try{
              $foundPost->posts_Interactions()->delete();
              $comments= $foundPost->comments()->get('id')->toArray();
              CommentReplay::whereIn('comment_id',$comments)->delete();
                 $foundPost->comments()->delete();

                 $foundPost->delete();
                 DB::commit();
             
             $this->DeleteFilesStorage($postImages);
             $this->DeleteFilesStorage($postVideos);

                 return  response()->json(['message' => 'success delete  post'], 203);
         }catch(Exception){
              DB::rollBack();
               return  response()->json(['message' => 'rollback delete post'], 406);
         }
               
             
        }
        else{
          return  response()->json(['message' => 'post not to you ,can not delete'], 405);
        }
      }else{
        return  response()->json(['message' => 'post not found'], 404);
      }
    }
    public function allPostToUser(Request $request){
      
     return Post::where('user_id',$request->user_id)->withCount('comments','posts_Interactions')->get();
    }
    public function allToOnePost(Request $request){
      
     return Post::where('id',$request->post_id)->with('users','comments.commentsReplaies','posts_Interactions')->get();
    }
  }
        
     
     
    