<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentReplyRequest;
use App\Http\Requests\DeleteReplyRequest;
use App\Models\Comment;
use App\Models\CommentReplay;
use Exception;
use Illuminate\Http\Request;

class CommentReplayController extends Controller
{
    public function Add(CommentReplyRequest $request){
         $foundcomment = Comment::find($request->comment_id);
         
           if ($foundcomment != null ) {
              try{
                $reply=[
			           'content' => $request->content,
                  'comment_id' =>  $request->comment_id,
                  'user_id' =>  $request->user()->id ];
      
                CommentReplay::create($reply);
                return  response()->json(['message' => 'success add comment reply'], 201);
               } 
             catch(Exception ){
                    return  response()->json(['message' => 'error in add comment reply'], 401);}
              } else {
               return  response()->json(['message' => 'comment not found'], 404);
           }
        }
    public function Delete(DeleteReplyRequest $request){
     
      $foundReply = CommentReplay::find($request->reply_id);
       if($foundReply != null) {
        if ($foundReply->user_id == $request->user()->id || $request->user()->role == 'admin')  {
         
          $foundReply->delete();
          return  response()->json(['message' => 'success delete  reply'], 203);
        }
        else{
          return  response()->json(['message' => 'reply not to you ,can not delete'], 405);
        }
      }else{
        return  response()->json(['message' => 'reply not found'], 404);
      }
    }
}