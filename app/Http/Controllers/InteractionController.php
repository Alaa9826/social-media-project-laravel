<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeletePostRequest;
use App\Http\Requests\InteractionRequest;
use App\Models\Interaction;
use App\Models\Post;
use App\Models\PostInteraction;
use Exception;
use Illuminate\Http\Request;

class InteractionController extends Controller
{
    public function Add(InteractionRequest $request){
         $foundPost = Post::find($request->post_id);
         $foundInteraction = Interaction::find($request->interaction_id);
           if ($foundPost != null && $foundInteraction != null ) {
               
              try{
                $interaction=[
			      'post_id' => $request->post_id,
                  'interaction_id' =>  $request->interaction_id,
                  'user_id' =>  $request->user()->id ];
    
         $foundPostInteraction = PostInteraction::where('user_id',$request->user()->id)->where('post_id',$request->post_id)->first();
        
         if ($foundPostInteraction != null) {
            
           if($foundPostInteraction->interaction_id == $request->interaction_id){
                return  response()->json(['message' => 'you are already interaction with this type in this post'], 405);
            }else{
                  $foundPostInteraction->update($interaction);
                  return  response()->json(['message' => 'success change your interaction'], 202); }
   
        } else {
                PostInteraction::create($interaction);
                return  response()->json(['message' => 'success add your interaction'], 201);}
               } 
             catch(Exception ){
                    return  response()->json(['message' => 'error in add Interaction'], 401);}
              } else {
               return  response()->json(['message' => 'post not found || Interaction not found'], 404);
           }
        }
  public function Delete(DeletePostRequest $request){
     $foundPost = Post::find($request->post_id);
       if($foundPost != null) {
      $foundPostInteraction = PostInteraction::where('user_id',$request->user()->id)->where('post_id',$request->post_id)->first();
       if($foundPostInteraction != null) {
        if ($foundPostInteraction->user_id == $request->user()->id || $request->user()->role == 'admin')  {
         
          $foundPostInteraction->delete();
          return  response()->json(['message' => 'success delete  interaction'], 203);
        }
        else{
          return  response()->json(['message' => 'interaction not to you ,can not delete'], 405);
        }
      }else{
        return  response()->json(['message' => 'you not have interaction in this post'], 404);
      }
    }else{
        return  response()->json(['message' => 'post not found'], 404);  }
}
     public function allPostInteraction(DeletePostRequest $request){
           return PostInteraction::where('post_id',$request->post_id)->with('interactions','users')->get();
       }
}