<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Models\User;
use GuzzleHttp\Psr7\Request;

class UserController extends Controller
{
public function  Register (RegisterRequest $request){
$foundUser=User::where('email',$request->email)->first();
        if ($foundUser == null) {

           $user = User::create([
    	        	'name' => $request->name,
    		       'email' => $request->email,
    		       'password' => Hash::make($request->password),
                   'role'=> $request->role  ]);

           $dataToken=['id' => $user->id,
                     'name' => $user->name,
    		         'email' => $user->email,
                     'role' => $user->role ];

          $token = $user->createToken($dataToken,[$user->role]);

    	return response()->json(['message'=> "success register",
                                 'token'=> $token ], 200);
        } else {
            return response()->json(['messages' => 'you are already register'], 405);
            }
        
    }
public function Login(LoginRequest $request){

         //$credentials = $request->only(['email','password','role']);
         $foundUser = User::where('email',$request->email)->where('role',$request->role)->first();
      
         if ($foundUser == true) {
           //  /** @var \App\Models\User $user **/
            //  $user = Auth::user();
             if (Hash::check($request->password, $foundUser->password)) {
                
                 $dataToken=['id' => $foundUser->id,
                            'name' => $foundUser->name,
    		                'email' => $foundUser->email,
                            'role' => $foundUser->role ];
                 
                $token = $foundUser->createToken($dataToken,[$foundUser->role]);

                return response()->json(['message'=> "success register",
                                         'token'=> $token ], 200);
             } else {
                  return response()->json(['message'=> "error password"], 411);
             }}
         else {
            return response()->json(['messages' => 'error you are not register'], 405);
        }
    }
    public function logout(){
        
      if(Auth::check()) {
            Auth::user()->token()->revoke();
            return response()->json(["status" => "success", "error" => false, "message" => "Success! You are logged out."], 200);
        }
        return response()->json(["status" => "failed", "error" => true, "message" => "Failed! You are already logged out."], 403);
    }
    public function GetAll(){
        return response()->json(['message' => User::all()], 200);
    }
}