<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Http\Requests\DeleteCommentRequest;
use App\Http\Requests\DeletePostRequest;
use App\Models\Comment;
use App\Models\Post;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    public function Add(CommentRequest $request){
         $foundPost = Post::find($request->post_id);
         
           if ($foundPost != null ) {
              try{
                $comment=[
			      'content' => $request->content,
                  'post_id' =>  $request->post_id,
                  'user_id' =>  $request->user()->id ];
      
                Comment::create($comment);
                return  response()->json(['message' => 'success add comment'], 201);
               } 
             catch(Exception ){
                    return  response()->json(['message' => 'error in add comment'], 401);}
              } else {
               return  response()->json(['message' => 'post not found'], 404);
           }
        }
     public function Delete(DeleteCommentRequest $request){
     
      $foundComment = Comment::find($request->comment_id);
       if($foundComment != null) {
        if ($foundComment->user_id == $request->user()->id || $request->user()->role == 'admin')  {
         
           DB::beginTransaction();
         try{
              $foundComment->commentsReplaies()->delete();
              $foundComment->delete();
                 DB::commit();
                  return  response()->json(['message' => 'success delete  comment and Replies'], 203);
         }catch(Exception){
              DB::rollBack();
               return  response()->json(['message' => 'rollback delete comment'], 406);
         }
            
        }
        else{
          return  response()->json(['message' => 'comment not to you ,can not delete'], 405);
        }
      }else{
        return  response()->json(['message' => 'comment not found'], 404);
      }
    }
    public function allCommentToPost(DeletePostRequest $request){
      return Comment::where('post_id',$request->post_id)->with('users','commentsReplaies.users')->get();
    }
  }