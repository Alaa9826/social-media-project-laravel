<?php

namespace App\Http\Traits;

/**
 * UploadFiles
 */
trait MyUploadFiles
{
    public function UploadImage($request,$fileName){
      if($request->hasFile($fileName)) {
        $images=$request->file($fileName);
         $uploadImages=[];
       foreach ($images as $key => $image) {
         $image_name=rand().'.'.$image->getClientOriginalName();
         $image->move(public_path('images'), $image_name);
        $uploadImages[]='images/'.$image_name;
       }
       return implode(',',$uploadImages);
    }
  else{
    return '';
  }}

    public function UploadVideo($request,$fileName){
      if($request->hasFile($fileName)) {
        $videos=$request->file($fileName);
         $uploadVideoa=[];
       foreach ($videos as $key => $video) {
         $video_name=rand().'.'.$video->getClientOriginalName();
         $video->move(public_path('videos'), $video_name);
        $uploadVideoa[]='videos/'.$video_name;
       }
       return implode(',',$uploadVideoa);
    }
  else{
    return '';
  }}
  public function DeleteFilesStorage($files){
             $allFiles=explode(',',$files);
              foreach ($allFiles as $key => $value) {
                 unlink(public_path($value));
              }
  }
}