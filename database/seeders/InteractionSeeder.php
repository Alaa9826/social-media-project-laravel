<?php

namespace Database\Seeders;

use App\Models\Interaction;
use Illuminate\Database\Seeder;

class InteractionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types=[['type'=>'happy'],['type'=>'sad'],['type'=>'like']];
        foreach ($types as $key => $value) {
            Interaction::create($value);
        }
    }
}