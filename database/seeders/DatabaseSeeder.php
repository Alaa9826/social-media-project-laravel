<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Models\CommentReplay;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
   /* \App\Models\User::factory(3)->has(
                      \App\Models\Post::factory(2) ->has(
                             \App\Models\Comment::factory(1))
     )->create();
     \App\Models\Comment::factory() ->has(
                                 \App\Models\CommentReplay::factory(1))->create();*/

     $this->call([
        InteractionSeeder::class]); 
        
      User::factory(3)->create()->each(function($user) {
          $post= Post::factory()->create(['user_id' => $user ]);
          $comment= Comment::factory()->create(['user_id' => $user,'post_id' => $post ]);
          $commentReplay= CommentReplay::factory()->create(['user_id' => $user,'comment_id' => $comment ]);
          
      });
       
    }
}